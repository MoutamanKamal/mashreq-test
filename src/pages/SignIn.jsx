import { useRef } from "react"
import { useToasts } from "react-toast-notifications"
import { useAuthContext } from "../contexts/AuthContext"
import { useHistory } from "react-router-dom"
import InputField from "../components/Fields/InputField"
export default function SignIn() {
  let history = useHistory()
  const mockDB = { username: "user", password: "password" }
  const authContext = useAuthContext()
  const usernameEl = useRef(null)
  const passwordEl = useRef(null)

  const { addToast } = useToasts()
  function login(data) {
    if (data.username !== mockDB.username)
      return { status: 404, data: {}, message: "User not found" }
    else if (data.password !== mockDB.password)
      return { status: 404, data: {}, message: "Wrong password" }
    else return { status: 200, data: mockDB }
  }
  function handleSubmit(e) {
    e.preventDefault()
    const payload = {
      username: usernameEl.current.value,
      password: passwordEl.current.value,
    }
    let response = login(payload)
    if (response.status !== 200) {
      addToast(response.message, { appearance: "error" })
      return
    }
    authContext.setState({ loggedIn: true })
    history.push("/dashboard")
  }
  return (
    <div className="w-full h-full flex items-center justify-center">
      <div className="max-w-1/2">
        <form
          className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4"
          onSubmit={handleSubmit}
        >
          <div className="mb-4">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="username"
            >
              Username
            </label>
            <InputField
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              type="text"
              ref={usernameEl}
              placeholder="Username"
              required
            />
          </div>
          <div className="mb-6">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="password"
            >
              Password
            </label>
            <InputField
              className="shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
              type="password"
              ref={passwordEl}
              placeholder="Password"
              required
            />
          </div>
          <div className="flex items-center justify-center">
            <button
              className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline transition-all duration-300 ease-in-out"
              type="submit"
            >
              Sign In
            </button>
          </div>
        </form>
      </div>
    </div>
  )
}
