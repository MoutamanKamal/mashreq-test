import { useEffect } from "react"
import useSWR from "swr"

import fetcher from "../../utils/fetcher"
import { useSearchContext } from "../../contexts/SearchContext"

import { useParams } from "react-router-dom"
import Loader from "../../components/Misc/Loader"
import Error from "../../components/Misc/Error"
import AlbumsList from "../../components/Albums/AlbumsList"

export default function Albums({ offset }) {
  let { userId } = useParams()
  const { data: userData, error: userError } = useSWR(
    `https://jsonplaceholder.typicode.com/users/${userId}`,
    fetcher
  )
  const { data, error } = useSWR(
    `https://jsonplaceholder.typicode.com/albums/?userId=${userId}&_start=${offset}&_limit=10`,
    fetcher
  )
  const searchContext = useSearchContext()

  useEffect(() => {
    searchContext.setState({
      title: `${userData?.name}'s Albums`,
      data: data,
      needle: ["title"],
    })
    searchContext.setFilterdData(data || [])
  }, [data])

  if (error || userError) return <Error />
  if (!data) return <Loader />

  return <AlbumsList data={searchContext.filterdData} />
}
