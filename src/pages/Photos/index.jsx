import { useState, useEffect } from "react"

import useSWR from "swr"
import fetcher from "../../utils/fetcher"
import { useSearchContext } from "../../contexts/SearchContext"

import { useParams } from "react-router-dom"
import Loader from "../../components/Misc/Loader"
import Error from "../../components/Misc/Error"

import { LazyLoadImage } from "react-lazy-load-image-component"
export default function Albums({ offset }) {
  let { albumId } = useParams()
  const { data: albumData, error: albumError } = useSWR(
    `https://jsonplaceholder.typicode.com/albums/${albumId}`,
    fetcher
  )
  const { data, error } = useSWR(
    `https://jsonplaceholder.typicode.com/photos/?albumId=${albumId}&_start=${offset}&_limit=10`,
    fetcher
  )

  const searchContext = useSearchContext()

  useEffect(() => {
    searchContext.setState({
      title: `Photos in "${albumData?.title}" Album`,
      data: data,
      needle: ["title"],
    })
    searchContext.setFilterdData(data || [])
  }, [data])


  if (error || albumError) return <Error/>
  if (!data) return <Loader />


  return (
    <section className="pt-8">
      <div className="flex flex-wrap -mx-4">
        {searchContext.filterdData.map((photo) => (
          <div key={photo.id} className="md:w-1/3 px-4 mb-8">
            <LazyLoadImage
              className="rounded shadow-md"
              src={photo.url}
              alt={photo.title}
            />
            <p>{photo.title}</p>
          </div>
        ))}
      </div>
    </section>
  )
}
