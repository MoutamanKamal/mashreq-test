import { useEffect } from "react"
import useSWR from "swr"

import fetcher from "../../utils/fetcher"
import { useSearchContext } from "../../contexts/SearchContext"

import Loader from "../../components/Misc/Loader"
import Error from "../../components/Misc/Error"
import UsersList from "../../components/Users/UsersList"

export default function Users({ offset }) {
  const { data, error } = useSWR(
    `https://jsonplaceholder.typicode.com/users?_start=${offset}&_limit=10`,
    fetcher
  )
  const searchContext = useSearchContext()

  useEffect(() => {
    searchContext.setState({
      title: "Dashboard",
      data: data,
      needle: ["name", "email"],
    })
    searchContext.setFilterdData(data || [])
  }, [data])

  if (error) return <Error />
  if (!data) return <Loader />

  return <UsersList data={searchContext.filterdData} />
}
