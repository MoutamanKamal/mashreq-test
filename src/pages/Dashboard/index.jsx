import { useState } from "react"
import { Switch, Route, useRouteMatch } from "react-router-dom"
import Albums from "../Albums"
import Photos from "../Photos"
import Users from "../Users"
import { Pagination } from "../../components/Misc/Pagination"
import { SearchWrapper } from "../../contexts/SearchContext"
export default function Dashboard() {
  const [offset, setOffset] = useState(0)
  let match = useRouteMatch()

  const pageCount = 5
  const perPage = 10

  function handlePagination({ selected }) {
    let newOffset = Math.ceil(selected * perPage)
    setOffset(newOffset)
  }
  return (
    <>
      <SearchWrapper>
        <Switch>
          <div className="container">
            <Route exact path={`${match.path}/:userId/albums/:albumId/photos`}>
              <Photos offset={offset} />
            </Route>
            <Route exact path={`${match.path}/:userId/albums`}>
              <Albums offset={offset} />
            </Route>
            <Route exact path={`${match.path}`}>
              <Users offset={offset} />
            </Route>
          </div>
        </Switch>
        <Pagination pageCount={pageCount} handlePagination={handlePagination} />
      </SearchWrapper>
    </>
  )
}
