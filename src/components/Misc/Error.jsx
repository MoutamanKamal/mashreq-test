export default function Error(params) {
  return (
    <div className="bg-red-100 text-red-400 font-bold my-10 text-center p-5 rounded-md text-xl">
      Failed to load
    </div>
  )
}
