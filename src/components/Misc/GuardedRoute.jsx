import React from "react"
import { Route, Redirect } from "react-router-dom"
import { useAuthContext } from "../../contexts/AuthContext"

const GuardedRoute = ({ component: Component, ...rest }) => {
  const authContext = useAuthContext()
  const auth = authContext.state.loggedIn
  return (
    <Route
      {...rest}
      render={(props) =>
        auth === true ? <Component {...props} /> : <Redirect to="/" />
      }
    />
  )
}

export default GuardedRoute
