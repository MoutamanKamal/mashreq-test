import { StageSpinner } from "react-spinners-kit"

export default function Loader() {
  return (
    <div
      className={`absolute left-0 top-0 w-screen h-screen flex justify-center items-center block"`}
    >
      <div className="fixed top-0 left-0 bg-black bg-opacity-50 h-screen w-screen z-40 transition-all ease-in-out duration-300"></div>

      <div className="relative z-50">
        <StageSpinner size={50} color="#fff" loading={true} />
      </div>
    </div>
  )
}
