import { Link } from "react-router-dom"

export default function AlnumsList({ data }) {
  if (data.length === 0)
    return (
      <div className="block border-b list-none rounded-md px-3 py-5 my-2 bg-white hover:shadow-md transition-all duration-300 ease-in-out">
        <p className="font-bold text-indigo-700">No data available</p>
      </div>
    )
  return (
    <ul>
      {data.map((album) => (
        <li key={album.id}>
          <Link
            to={`albums/${album.id}/photos`}
            className="block border-b list-none rounded-md px-3 py-5 my-2 bg-white hover:shadow-md transition-all duration-300 ease-in-out"
          >
            <p className="font-bold text-indigo-700">{album.title}</p>
          </Link>
        </li>
      ))}
    </ul>
  )
}
