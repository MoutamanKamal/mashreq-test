import { Link } from "react-router-dom"

export default function UsersList({ data }) {
  if (data.length === 0)
    return (
      <div className="block border-b list-none rounded-md px-3 py-5 my-2 bg-white hover:shadow-md transition-all duration-300 ease-in-out">
        <p className="font-bold text-indigo-700">No data available</p>
      </div>
    )
  return (
    <ul>
      {data.map((user) => (
        <li key={user.id}>
          <Link
            to={`/dashboard/${user.id}/albums`}
            className="block border-b list-none rounded-md px-3 py-5 my-2 bg-white hover:shadow-md transition-all duration-300 ease-in-out"
          >
            <p className="font-bold text-indigo-700">
              {user.name}{" "}
              <span className="text-sm font-medium text-gray-700 mx-1">
                {user.email}
              </span>
            </p>
            <p className="text-sm font-medium text-gray-700">
              @{user.username}
            </p>
          </Link>
        </li>
      ))}
    </ul>
  )
}
