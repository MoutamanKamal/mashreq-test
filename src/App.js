import React from "react"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import "./styles/main.css"
import Dashboard from "./pages/Dashboard"
import SignIn from "./pages/SignIn"
import GuardedRoute from "./components/Misc/GuardedRoute"
import { ToastProvider } from "react-toast-notifications"
import { AuthWrapper } from "./contexts/AuthContext"

function App({ pageProps }) {
  return (
    <AuthWrapper {...pageProps}>
      <ToastProvider>
        <Router>
          <div className="App w-full h-full bg-gray-50">
            <div className="container mx-auto h-full ">
              <Switch>
                <GuardedRoute path="/dashboard" component={Dashboard} />
                <Route path="/">
                  <SignIn />
                </Route>
              </Switch>
            </div>
          </div>
        </Router>
      </ToastProvider>
    </AuthWrapper>
  )
}

export default App
