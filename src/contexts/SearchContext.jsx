import { createContext, useState, useEffect, useContext } from "react"
import { useHistory } from "react-router-dom"

import Search from "../components/Fields/Search"
const SearchContext = createContext()

export function SearchWrapper({ children }) {
  const [state, setState] = useState({ title:"",data: [], needle: "", filterData: "" })
  const [filterdData, setFilterdData] = useState([])
  const history = useHistory()

  useEffect(() => {
    setFilterdData(state.data || [])
  }, [state.data])
  function filterData(data) {
    setFilterdData(data)
  }
  return (
    <SearchContext.Provider value={{ state, setState,filterdData, setFilterdData }}>
      <div className=" min-h-full w-full pt-20">
        <header className="flex flex-row justify-between items-center">
          <button
            className="bg-blue-200 p-2 px-5 rounded-md text-blue-800 font-semibold hover:bg-blue-800 hover:text-blue-200 transition-all duration-300 ease-in-out"
            onClick={() => history.goBack()}
          >
            Back
          </button>
          <h1 className="text-2xl font-bold">{state.title}</h1>
          <div>
            <Search
              data={state.data}
              result={filterData}
              needle={state.needle}
            />
          </div>
        </header>
        {children}
      </div>
    </SearchContext.Provider>
  )
}
export function useSearchContext() {
  return useContext(SearchContext)
}
