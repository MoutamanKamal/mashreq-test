import { createContext, useState, useContext } from "react"
const AuthContext = createContext()

export function AuthWrapper({ children }) {
  const [state, setState] = useState({ loggedIn: false })

  return (
    <AuthContext.Provider value={{ state, setState }}>
      {children}
    </AuthContext.Provider>
  )
}
export function useAuthContext() {
  return useContext(AuthContext)
}
